package Tarefa3;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClient extends Remote {
	
	void setUser(String login) throws RemoteException;
	String getUser() throws RemoteException;
	void setMsg(String msg) throws RemoteException;
	String getMsg() throws RemoteException;
}
