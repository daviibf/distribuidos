package Tarefa3;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Interface extends Remote {
	
	void setMsg(String str) throws RemoteException;
	String getMsg() throws RemoteException;
	
}
