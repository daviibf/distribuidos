package Tarefa3;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;


public class Client implements IClient {

    private Client() {}
    
    public String login;
    public String msg;
    
   public void setUser(String login) {
	   this.login = login;
   }
   
   public String getUser() {
	   return login;
   }
   
   public void setMsg(String msg) {
	   this.msg = msg;
   }
   
   public String getMsg() {
	   return msg;
   }
   
   
   public static void main(String[] args) {
    	
    	Client obj = new Client();
    	
    	Scanner entrada = new Scanner(System.in);
    	
    	System.out.println("Vocer que logar? - Responda 'S' ou 'N'");
    	String resp = entrada.nextLine();
    	
    	if(resp == "S") {
    	
	    	System.out.println("Escolha um nome de usuario:");
	    	String login = entrada.nextLine();
	    	obj.setUser(login);
	    	
	    	try {
	         	
	         	IClient stub = (IClient) UnicastRemoteObject.exportObject(obj, 0);
	         	Registry registry = LocateRegistry.createRegistry(1099);
	            registry.bind( obj.getUser() , stub );
	
	         } catch (Exception e) {
	             System.err.println("Client exception: " + e.toString());
	             e.printStackTrace();
	         }
	    	
	    	System.out.println("Digite a msg a ser enviada:");
	    	String msg = entrada.nextLine();
	    	obj.setMsg(msg);
	    	
	       while(msg == null) {
	    	try {
	        	Registry registry = LocateRegistry.getRegistry(1099);
	        	Interface stub = (Interface) registry.lookup("Servidor");
	            stub.setMsg(msg);
	            if ( obj.getMsg() != stub.getMsg()) {
	            	System.out.println(obj.getMsg());
	            }
	            
	        } catch (Exception e) {
	            System.err.println("Client exception: " + e.toString());
	            e.printStackTrace();
	        }	
	       }	
	    }else
	    		
	    	System.out.println("Quem sabe mais tarde...");
    	
    	
    }
}