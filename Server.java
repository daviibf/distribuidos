package Tarefa3;

import java.rmi.registry.Registry;
import java.rmi.registry.LocateRegistry;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
        
public class Server implements Interface {
        
    public Server() {}
    
    public String msg;

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    public String getMsg() {
        return msg;
    }
        
    public static void main(String args[]) {
        
    	try {
        	
        	// Bind the remote object's stub in the registry
        	
        	Server obj = new Server();
        	Interface stub = (Interface) UnicastRemoteObject.exportObject(obj, 0);
        	Registry registry = LocateRegistry.createRegistry(1099);
            registry.bind("Servidor", stub);
            
  
            System.err.println("Server ready");
            
        } catch (Exception e) {
        	
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
            
        }
    }
}