import threading 
import numpy as np
from random import randint 

i1 = randint(1,15)
i2 = randint(1,15)
j = randint(1,15)
thr = []
matrizR = np.zeros([i1, i2]);

matriz1 = np.random.random_integers(0 + 1, 99 + 1, (i1, j))
matriz2 = np.random.random_integers(0 + 1, 99 + 1, (j, i2))

def prodM(linha , coluna):
    arrayL = matriz1[linha,:]
    arrayC = matriz2[:,coluna]
    val = 0
    
    for i in range(len(arrayL)):
        val += arrayL[i]*arrayC[i]
    
    matrizR[linha, coluna] = val

for w in range(len(matriz1)):
    for z in range(len(matriz2[0])):
        thr.append(threading.Thread(target = prodM , args = (w, z)))
        
for k in range(len(thr)):
    thr[k].start()
    
for q in range(len(thr)):
    thr[q].join()
    
print(matrizR)
    